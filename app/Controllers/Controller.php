<?php

namespace App\Controllers;

use Slim\Router;
use Slim\Views\Twig as View;

class Controller
{
  protected $router;

  protected $view;

  public function __construct(Router $router, View $view)
  {
    $this->router = $router;
    $this->view   = $view;
  }
}
