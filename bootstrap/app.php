<?php

ini_set('display_errors', 'On');

session_start();

use App\App;
use Illuminate\Database\Capsule\Manager as Capsule;

require __DIR__ . '/../vendor/autoload.php';

$app = new App;

$dotenv = new Dotenv\Dotenv(__DIR__);

if (getenv('APP_ENV') === 'development') {
    $dotenv->load(__DIR__);
}

$capsule = new Capsule;
$capsule->addConnection([
  'driver'    => getenv('DB_DRIVER'),
  'host'      => getenv('DB_HOST'),
  'database'  => getenv('DB_DATABASE'),
  'username'  => getenv('DB_USERNAME'),
  'password'  => getenv('DB_PASSWORD'),
  'charset'   => 'utf8',
  'collation' => 'utf8_unicode_ci',
  'prefix'    => ''
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();

require __DIR__ . '/../routes/web.php';
